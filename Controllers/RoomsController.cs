﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectAPI.Models;

namespace ProjectAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RoomsController : Controller
    {

        private ProjectDBContext _dbContext;

        public RoomsController(ProjectDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public async Task<IEnumerable<Room>> Get()
        {
            return await _dbContext.Rooms.ToListAsync();
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] Room room)
        {
            _dbContext.Rooms.Add(room);
            await _dbContext.SaveChangesAsync();
            return StatusCode(201, room);
        }
    }
}
