﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectAPI.Models;

namespace ProjectAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HotelsController : Controller
    {
        private ProjectDBContext _dbContext;

        public HotelsController(ProjectDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public async Task<IEnumerable<Hotel>> Get()
        {
            return await _dbContext.Hotels.ToListAsync();
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] Hotel hotel)
        {
            _dbContext.Hotels.Add(hotel);
            await _dbContext.SaveChangesAsync();
            return StatusCode(201, hotel);
        }
    }
}
