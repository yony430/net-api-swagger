﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAPI.Models
{
    public class Booking
    {
        public Guid Id { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        [ForeignKey("IdRoom")]
        public Room Room { get; set; }
        private Guid IdRoom;

        [ForeignKey("IdCustomer")]
        public Customer Customer { get; set; }
        private Guid IdCustomer;

    }
}
